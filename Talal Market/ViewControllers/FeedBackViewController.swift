//
//  FeedBackViewController.swift
//  Talal Market
//
//  Created by Sajin M on 12/09/2020.
//  Copyright © 2020 DataDorf. All rights reserved.
//

import UIKit
import TextFieldEffects

class FeedBackViewController: BaseViewController {
    
    
    @IBOutlet weak var txtName: HoshiTextField!
    @IBOutlet weak var txtMobileNumber: HoshiTextField!
    @IBOutlet weak var txtEmail: HoshiTextField!
    @IBOutlet weak var txtMessage: HoshiTextField!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtMobileNumber.delegate = self

        // Do any additional setup after loading the view.
    }
    

    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }
    
    @IBAction func submitPressed(_ sender: Any) {
        
        if isConnected(){
            
            
            
            guard let textEmail = self.txtEmail.text, let textMessage = self.txtMessage.text,let textName = self.txtName.text, let textPhone = self.txtMobileNumber.text,!textEmail.isEmpty,!textMessage.isEmpty,!textName.isEmpty,!textPhone.isEmpty else {
                
                self.showNotification(message: fillAllFields)
                
                return
            }
            
            
            if textPhone.count < 8{
                
                self.showNotification(message: notValidPhone)
                return
            }
            
          
            
            
            if textEmail.isValidEmail(){
                
                
                self.sendFeedBack(name: textName.capitalizingFirstLetter(), message: textMessage, phone:textPhone, email:textEmail )
                
                
                
            }else{
                
                self.showNotification(message: notValidEmail)
                
            }
            
            
            
        }
        
        
        
        
    }
    
    
    func sendFeedBack(name:String,message:String,phone:String,email:String){
        
        self.STProgress.show()
           
        let params = FeedbackParams.init(Name:name, Mobile: phone, Email: email, Message: message).Values
               ApiRequest.sendFeedback(withParameter:params) { (isSuccess,message) in
                   
                   self.STProgress.dismiss()
                
                DispatchQueue.main.async {
                    
                    self.txtMobileNumber.text = ""
                    self.txtMessage.text = ""
                    self.txtEmail.text = ""
                    self.txtName.text = ""
                    self.showAlert(message: message)
                    
                }
                
                self.backNavigation()
                
                
                
   
                
                   
               }
        
        
        
    }
    
    

}

extension FeedBackViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        // get the current text, or use an empty string if that failed
        let currentText = textField.text ?? ""

        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }

        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

        // make sure the result is under 16 characters
        return updatedText.count <= 8
    }

    
    
    
    
}
