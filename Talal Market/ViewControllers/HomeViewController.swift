//
//  HomeViewController.swift
//  Talal Market
//
//  Created by Sajin M on 09/09/2020.
//  Copyright © 2020 DataDorf. All rights reserved.
//

import UIKit
import FSPagerView
import Kingfisher
import CoreLocation
import SystemConfiguration


class HomeViewController:BaseViewController{
    
    
    var locationLat: Double = 0
    var locationLong: Double = 0
    
    var storeLat:String?
    var storeLong:String?
    var shopArray:[String:[storeStruct]] = [:]
    var emirateArray:[String] = []
    var shopList:[[String]] = []
    var bannerData:[BannerData]?
    var storeData:StoreModel?
    var storesList:[storeStruct]?
    var storeName:String? = ""
    
  
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    
    private var locationManager:CLLocationManager?
    
    @IBOutlet weak var bannerView: FSPagerView!{
           didSet {
            
               self.bannerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: bannerCell)
           }
       }

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        initialSetup()
       
    }
    
    
    func initialSetup(){
        
         
        
        self.btnMenu.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControl.Event.touchUpInside)
        
             locationManager = CLLocationManager()
             locationManager?.delegate = self
             locationManager?.requestAlwaysAuthorization()
            
            if isConnected(){
                
                 if let fcm = defualts.string(forKey: "fcmToken"){
                
                 self.registerFcm(fcm:fcm)
            }
                
        self.getBanners()
      

            
 
        }
        
        
        
    }
    
    

    
    
    
    @IBAction func promotionPressed(_ sender: Any) {
        
        
         let flyerScene = FlyerViewController.instantiate(fromAppStoryboard: .Main)
        
                            if let navigator = self.navigationController {
                                
                                if let storeId = self.storeData?.store_id{
                                    
                                    if let storeName = self.storeData?.store_name{
                                        self.storeName = storeName
                                    }
                                    
                                    if let catId = self.storeData?.arrOfferCategory?[0].categoryId{
                                        if let title = self.storeData?.arrOfferCategory?[0].promoCategory{
                                            
                                            flyerScene.offerTitle = title
                                        }
                                        
                                        if let time = self.storeData?.arrOfferCategory?[0].promoTillDate{
                                            
                                            flyerScene.offerTime = time
                                        }
                                        flyerScene.storeId = storeId
                                        flyerScene.categoryId = "\(catId)"
                                        flyerScene.storeTitle = self.storeName
                                        navigator.pushViewController(flyerScene, animated: true)
                                        
                                    }
                                    
                                    
                                    
                                }else{
                                    
                                    let flyerScene = FlyerViewController.instantiate(fromAppStoryboard: .Main)
                                    
                                    if let navigator = self.navigationController {
                                        
                                        navigator.pushViewController(flyerScene, animated: true)
                                        
                                    }
                                    
                                    
                                }
                            
                                
                               
        
        
        
                            }
        
        
    }
    
    @IBAction func rewardPressed(_ sender: Any) {
        
        self.showAlert(message: "Coming Soon..")
    }
    
    @IBAction func nearMePressed(_ sender: Any) {
        
        
        guard let storeLat = self.storeData?.latitude,let storeLong = self.storeData?.longitude else{
            
            return
        }
        
        goToNearest(long: storeLong, lat: storeLat)
        
    }
   
    
    func StoreWithID(storeId:String){
        
        self.STProgress.show()
        
        
        
        ApiRequest.getStore(withParameter:["store_id":storeId]) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
             
             if let info = self.ApiRequest.NearestLocationResponse{
                 
                 self.storeData = info
                 
                 if let location = self.storeData?.store_address{
                     
                     self.lblLocation.text = location
                 }
                 
                 
             }
                
                
               
                
            }
            
        }
        
        
    }
    
       func getNearestStore(long:String,lat:String){
           
           self.STProgress.show()
           
           let Params = NearestParams(lattitude:lat,longitude:long).Values
           
           ApiRequest.getNearestLocation(withParameter:Params) { (isSuccess,message) in
               
               self.STProgress.dismiss()
               
               if isSuccess {
                   
                
                if let info = self.ApiRequest.NearestLocationResponse{
                    
                    self.storeData = info
                    
                    if let location = self.storeData?.store_address{
                        
                        self.lblLocation.text = location
                    }
                    
                    
                }
                   
                   
                  
                   
               }
               
           }
           
           
       }
    
    
    
    @IBAction func showOutLetsPressed(_ sender: Any) {
        
        let newScene = StoreMapViewController.instantiate(fromAppStoryboard: .Main)
        if let navigator = self.navigationController {
            
            navigator.pushViewController(newScene, animated: true)
            
        }
        
    }
    
    
    
    func goToNearest(long:String,lat:String){


        if  UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")! as URL){
        UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(lat)),\(long)&zoom=14&views=traffic&q=\(lat),\(long)")!, options: [:], completionHandler: nil)

             }else{


                  UIApplication.shared.openURL(NSURL(string: "itms://itunes.apple.com/us/app/google-maps-transit-food/id585027354?mt=8")! as URL)
        }

    }
    
    
    func getBanners(){
        
        
        self.STProgress.show()
        

        
        ApiRequest.getBanner(withParameter:["storeId":"1"]) { (isSuccess,bannerData,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
                
                if let info = bannerData {
                    
          
                    if info.count > 0 {
                
                    self.bannerData = info
                    self.bannerView.delegate = self
                    self.bannerView.dataSource = self
                    self.bannerView.reloadData()
                    
                }
                
                
            }
            
        }
        
     }
    }

    func registerFcm(fcm:String){
        
        let params = FcmRegisterParam(fcm_regId:fcm).Values
                             
                             ApiRequest.registerFCM(withParameter: params) { (isSuccess,message) in
                              
                               self.STProgress.dismiss()
                                             
                                             if isSuccess {
                                                 
                                                return

                                             } else{
                                               
                                              return
                                               
                               }
        }
        
    }

}

extension HomeViewController: FSPagerViewDelegate,FSPagerViewDataSource {

func numberOfItems(in pagerView: FSPagerView) -> Int {
    return self.bannerData?.count ?? 0
}
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        
        
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: bannerCell, at: index)
        
       
      
        if let info = self.bannerData?[index]{
            
            if let imgUrl = info.bannerImage {
                
                
                let url = URL(string: imgUrl)
                let phImage = UIImage(named:"phPort")
                cell.imageView?.contentMode = .redraw
                cell.imageView?.kf.setImage(with: url, placeholder: phImage)
                
            }
            
            
        }
        
        return cell
        
        
    }

}

extension HomeViewController:CLLocationManagerDelegate{
    

    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        
     
                              if let location = locations.last {
                      
                                  if location != nil{
                      
                      
                                      self.locationLat = location.coordinate.latitude
                                      self.locationLong = location.coordinate.longitude
                                    
                                    
                                    if isConnected(){
                                            getNearestStore(long:"\(self.locationLong)",lat:"\(self.locationLat)")

                                    }else{
                                        
                                        self.STProgress.dismiss()
                                        
                                        
                                    }
                      
                      
                                      
                      
                      
                      
                                  }
                      
                              }else{
                                
                                self.STProgress.dismiss()
                               
                                
        }
             
        
                              self.locationManager?.stopUpdatingLocation()
                              self.locationManager?.delegate = nil
                             
      

      }
    
    
    
    
    
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.STProgress.dismiss()
      
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("location manager authorization status changed")
        
        switch status {
        case .authorizedAlways:
            
            locationManager?.startUpdatingLocation()
            locationManager?.delegate = self
            
           // retriveCurrentLocation()
            print("user allow app to get location data when app is active or in background")
        case .authorizedWhenInUse:
            
            locationManager?.startUpdatingLocation()
            locationManager?.delegate = self
            
            print("user allow app to get location data only when app is active")
        case .denied:
            
             locationManager?.requestAlwaysAuthorization()
             self.STProgress.dismiss()
            
            print("user tap 'disallow' on the permission dialog, cant get location data")
        case .restricted:
            
            self.STProgress.dismiss()
           
            print("parental control setting disallow location data")
        case .notDetermined:
            
             self.STProgress.dismiss()
            
            print("the location permission dialog haven't shown before, user haven't tap allow/disallow")
        default:
            break
        }
    }
    
    
    
    
    
}

//extension HomeViewController:UITableViewDelegate,UITableViewDataSource{
//    
//    
//      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//            
//
//           
//            
//        let info = self.shopArray[self.emirateArray[section]]
//     
//            
//            return info?.count ?? 0
//            
//
//            
//        }
//        
//        func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
//               view.tintColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
//               let header = view as! UITableViewHeaderFooterView
//            
//               header.textLabel?.textColor = #colorLiteral(red: 0, green: 0.6461210847, blue: 0.2761240602, alpha: 1)
//
//           }
//           
//           func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//               
//               return 45.0
//           }
//           
//           func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//               
//               let cell = shopListingTable.dequeueReusableCell(withIdentifier: "shopNameCell", for: indexPath) as! NameTableCell
//               
//               
//              
//            let info = self.shopArray[self.emirateArray[indexPath.section]]
//               
//             
//            if let store = info?[indexPath.row].storeName{
//                
//               cell.lblStoreName.text = store
//            }
//                
//        
//               return cell
//               
//           }
//           
//
//        
//        func numberOfSections(in tableView: UITableView) -> Int {
//            return self.emirateArray.count
//        }
//        
//        func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//          return  self.emirateArray[section]
//        }
//        
//        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//            
//            
//            
//           let info = self.shopArray[self.emirateArray[indexPath.section]]
//              
//            
//           if let storeid = info?[indexPath.row].id{
//             
//            self.StoreWithID(storeId: storeid)
//             
//           }
//
//
//            
//            self.shoplistingView.isHidden = true
//            
//
//            
//            
//            
//        }
//    
//    
//}

extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }

        return result
    }
}

public extension Sequence {
    func group<U: Hashable>(by key: (Iterator.Element) -> U) -> [U:[Iterator.Element]] {
        var categories: [U: [Iterator.Element]] = [:]
        for element in self {
            let key = key(element)
            if case nil = categories[key]?.append(element) {
                categories[key] = [element]
            }
        }
        return categories
    }
}
