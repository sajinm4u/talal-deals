//
//  FlyerViewController.swift
//  Talal Market
//
//  Created by Sajin M on 11/09/2020.
//  Copyright © 2020 DataDorf. All rights reserved.
//

import UIKit
import FSPagerView
import Kingfisher

class FlyerViewController: BaseViewController,FSPagerViewDataSource,FSPagerViewDelegate,UIScrollViewDelegate {
    
     @IBOutlet weak var pagerViewScroll: UIScrollView!
     
     @IBOutlet weak var promotionCollectionView: UICollectionView!
    
    @IBOutlet weak var promoTitle: UILabel!
    @IBOutlet weak var promoTime: UILabel!
    @IBOutlet weak var storeName: UILabel!
    
    @IBOutlet weak var shoplistingView: UIView!
    @IBOutlet weak var shopListingTable: UITableView!
    
    var offerTime:String?
    var offerTitle:String?
    var storeId:String?
    var categoryId:String?
    var bannerUrls:[String] = []
    var shopArray:[String:[storeStruct]] = [:]
    var emirateArray:[String] = []
    var shopList:[[String]] = []
    var storeData:StoreModel?
    var storesList:[storeStruct]?
    var storeTitle:String?

     
     
     @IBOutlet weak var pagerView: FSPagerView!
         {
         didSet {
             self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
             self.pagerView.isInfinite = false
             self.pagerView.automaticSlidingInterval = 0
             
             
         }
     }
    
    
    

    
    @IBAction func storePressed(_ sender: Any) {
        self.shoplistingView.isHidden = false
        
    }
    
    
    
    
    @IBAction func shopListClosePressed(_ sender: Any) {
          
          self.shoplistingView.isHidden = true
          
      }
    
    
    @IBAction func backPressed(_ sender: Any) {
        self.backNavigation()
    }
    
    
     @IBOutlet weak var btnGrid: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.shoplistingView.isHidden = true
        self.shopListingTable.tableFooterView = UIView(frame:.zero)
        pagerViewScroll.minimumZoomScale = 1.0
        pagerViewScroll.maximumZoomScale = 5.0
        self.promotionCollectionView.isHidden = true
        
        self.getStoresList()
        
        if let title = self.offerTitle {
            
            self.promoTitle.text = title
            
        }
        if let time = self.offerTime{
           
            self.promoTime.text = "Valid till: " + time
            
        }
        
        if let name = self.storeTitle{
            self.storeName.text = name
        }
        
        self.getNearestStore(long: "24.9965341", lat: "55.141819")
        
//        guard let storeId = self.storeId,let catId = self.categoryId else{
//            self.showAlert(message: "Flyer not available for this store.Please select any other store")
//            return
//        }
//
//        self.getStoreFlyers(catId:catId,StoreId:storeId)
    
        
        self.pagerViewScroll.isHidden = true
        self.promotionCollectionView.isHidden = false
        self.btnGrid.isHidden = true

        
    }
    
    @IBAction func gridViewPressed(_ sender: Any) {
         
         self.pagerViewScroll.isHidden = true
         self.promotionCollectionView.isHidden = false
         self.btnGrid.isHidden = true
     }
    
    func getStoresList(){
        
        self.STProgress.show()
    
        
        ApiRequest.getStoreList(withParameter:[:]) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
             
                if let info = self.ApiRequest.StoreListResponse?.stores{
                    
                    if info.count > 0{
                        
                        self.storesList = info
                        
           
                        for data in info
                        {
                            

                            if let location = data.storeLocation{
                             
                                self.emirateArray.append(location)
                            }
                  
                        }
                        
                   
                        self.emirateArray = self.emirateArray.removeDuplicates()

                        let check = Dictionary(grouping:self.storesList!, by: {$0.storeLocation!} )
                        
                        self.shopArray = check
                      
                        self.shopListingTable.delegate = self
                        self.shopListingTable.dataSource = self
                        self.shopListingTable.reloadData()
                        
                    }
                 
             }
                
                
               
                
            }
            
        }
        
        
    }
    
    
    func getNearestStore(long:String,lat:String){
        
        self.STProgress.show()
        
        let Params = NearestParams(lattitude:lat,longitude:long).Values
        
        ApiRequest.getNearestLocation(withParameter:Params) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
             
             if let info = self.ApiRequest.NearestLocationResponse{
                 
                 self.storeData = info
                
                if let storeId = self.storeData?.store_id{
                    
                    if let storeName = self.storeData?.store_name{
                        self.storeName.text = storeName
                    }
                    
                
                    
                    if let catId = self.storeData?.arrOfferCategory?[0].categoryId{
                        if let title = self.storeData?.arrOfferCategory?[0].promoCategory{
                            
                            self.getStoreFlyers(catId:"\(catId)",StoreId:"\(storeId)")
                            self.promoTitle.text = title
                        }
                        
                        if let time = self.storeData?.arrOfferCategory?[0].promoTillDate{
                            
                            self.promoTime.text = time
                        }
                    }
                
             }
                
                
             }
                
            }else{
                
                self.showAlert(message: "Flyer not available for this store.Please select any other store")
            }
            
        }
        
        
    }
    
    
    func StoreWithID(storeId:String){
        
        self.STProgress.show()
        
        
        
        ApiRequest.getStore(withParameter:["store_id":storeId]) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
             
             if let info = self.ApiRequest.NearestLocationResponse{
                 
                 self.storeData = info
                
                if let storeId = self.storeData?.store_id{
                    
                    if let storeName = self.storeData?.store_name{
                        self.storeName.text = storeName
                    }
                    
                    if let catId = self.storeData?.arrOfferCategory?[0].categoryId{
                        if let title = self.storeData?.arrOfferCategory?[0].promoCategory{
                            
                            self.getStoreFlyers(catId:"\(catId)",StoreId:"\(storeId)")
                            self.promoTitle.text = title
                        }
                        
                        if let time = self.storeData?.arrOfferCategory?[0].promoTillDate{
                            
                            self.promoTime.text = time
                        }
                    }
                
             }
                 
//                 self.storeData = info
//                 if info.count > 0{
//
//                     self.bannerUrls = info
//                     self.pagerView.dataSource = self
//                     self.pagerView.delegate = self
//                     self.pagerView.reloadData()
//                     self.promotionCollectionView.delegate = self
//                     self.promotionCollectionView.dataSource = self
//                     self.promotionCollectionView.reloadData()
//
//
//                 }
//
          
             }
          
            }
            
        }
        
        
    }
    
    
    func getStoreFlyers(catId:String,StoreId:String){
        
        self.STProgress.show()
        
        let Params = FlyerParams(store_id:StoreId,category_id:catId).Values
        
        ApiRequest.getFlyers(withParameter:Params) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
             
                if let info = self.ApiRequest.FlyerResponse{
                    
                    if info.count > 0{
                        
                        self.bannerUrls = info
                        self.pagerView.dataSource = self
                        self.pagerView.delegate = self
                        self.pagerView.reloadData()
                        self.promotionCollectionView.delegate = self
                        self.promotionCollectionView.dataSource = self
                        self.promotionCollectionView.reloadData()
                        
                        
                    }
      
             }
        
            }else{
                
                self.showAlert(message: "Flyer not available for this store.Please select any other store")
            }
            
        }
        
        
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
           return pagerView
       }
       

       
       public func numberOfItems(in pagerView: FSPagerView) -> Int {
           return bannerUrls.count
       }
       
       public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
           let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
           
           
           
           if !bannerUrls[index].isEmpty {
               let url = URL(string: bannerUrls[index].addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)

       
             cell.imageView?.kf.setImage(with: url, placeholder: talalPh)
           
               
           }
           
           
         
           
           return cell
       }
       

   

}

extension FlyerViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        

        return self.bannerUrls.count
        
    }
    
 
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.promotionCollectionView.dequeueReusableCell(withReuseIdentifier: "promotionCell", for: indexPath) as! PromotionViewCell
        

            if !bannerUrls[indexPath.row].isEmpty {
                    let url = URL(string: bannerUrls[indexPath.row].addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)

            
                  cell.imgBanner?.kf.setImage(with: url, placeholder: phImage)
                
                    
                }
           
           
            
             cell.lblTime.text = "Page " + "\(indexPath.row + 1)"
             
            
     return cell
      
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                  
                 
                  
                  let width = (collectionView.frame.size.width - 8 * 2) / 3
                let height = width * 1.35 //ratio
                  return CGSize(width: width, height: height)
                  
              
       }
       
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.pagerViewScroll.isHidden = false
        self.promotionCollectionView.isHidden = true
        self.btnGrid.isHidden = false
        self.pagerView.scrollToItem(at: indexPath.row, animated: false)
        
    }
    
    
    
}

extension FlyerViewController:UITableViewDelegate,UITableViewDataSource{
    
    
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
 
        let info = self.shopArray[self.emirateArray[section]]
     
            
            return info?.count ?? 0
            

            
        }
        
        func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
               view.tintColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
               let header = view as! UITableViewHeaderFooterView
            
               header.textLabel?.textColor = #colorLiteral(red: 0, green: 0.6461210847, blue: 0.2761240602, alpha: 1)

           }
           
           func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
               
               return 45.0
           }
           
           func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
               
               let cell = shopListingTable.dequeueReusableCell(withIdentifier: "shopNameCell", for: indexPath) as! NameTableCell
               
               
              
            let info = self.shopArray[self.emirateArray[indexPath.section]]
               
             
            if let store = info?[indexPath.row].storeName{
                
               cell.lblStoreName.text = store
            }
                
        
               return cell
               
           }
           

        
        func numberOfSections(in tableView: UITableView) -> Int {
            return self.emirateArray.count
        }
        
        func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
          return  self.emirateArray[section]
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            
            
           let info = self.shopArray[self.emirateArray[indexPath.section]]
            print(info![indexPath.row])
            
            
            if let id = info?[indexPath.row].id{
                self.StoreWithID(storeId: id)
            }
            
//           if let storeLat = info?[indexPath.row].storeLatitude{
//
//            if let storeLong = info?[indexPath.row].storeLongitude{
//
//                getNearestStore(long: storeLat, lat: storeLong)
//
//            }
//           }

            self.shoplistingView.isHidden = true
            
        }
    
    
}
