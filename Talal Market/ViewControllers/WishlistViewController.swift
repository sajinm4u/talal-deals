//
//  WishlistViewController.swift
//  Talal Deals
//
//  Created by Sajin M on 01/07/2021.
//  Copyright © 2021 DataDorf. All rights reserved.
//

import UIKit

class WishlistViewController: UIViewController {
    
    
    @IBOutlet weak var wishTableView: UITableView!
    
    let defaults = UserDefaults.standard
    var wishListArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.wishTableView.tableFooterView = UIView(frame: .zero)
        wishListArray = defaults.stringArray(forKey: "wishListArray") ?? [String]()
        
        if wishListArray.count > 0{

            self.wishTableView.delegate = self
            self.wishTableView.dataSource = self
            self.wishTableView.reloadData()
        }

        // Do any additional setup after loading the view.
    }
    
    
    func showAlert(){
        
        //1. Create the alert controller.
        let alert = UIAlertController(title: "", message: "Add To Wishlist", preferredStyle: .alert)

        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.text = ""
        }

        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "SAVE", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            guard let text = alert?.textFields![0].text, !text.isEmpty else{
                return
            }
            
            if !self.wishListArray.contains(text){
                self.wishListArray.append(text)
                self.wishTableView.delegate = self
                self.wishTableView.dataSource = self
                self.wishTableView.reloadData()
                self.defaults.set(self.wishListArray, forKey: "wishListArray")
            }
            
        }))
        
        let cancel = UIAlertAction(title: "CANCEL", style: .default) { (alertAction) in }
            alert.addAction(cancel)


        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func addPressed(_ sender: Any) {
        self.showAlert()
    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    

}

extension WishlistViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.wishListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.wishTableView.dequeueReusableCell(withIdentifier: "wishCell", for: indexPath) as! NameTableCell
        
        if self.wishListArray[indexPath.row] != nil{
            cell.lblStoreName.text = self.wishListArray[indexPath.row]
        }
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            self.wishListArray.remove(at: indexPath.row)
            self.defaults.set(self.wishListArray, forKey: "wishListArray")
            self.wishTableView.reloadData()
        }
    }
    
    
}
