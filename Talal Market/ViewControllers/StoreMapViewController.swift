//
//  StoreMapViewController.swift
//  Talal Market
//
//  Created by Sajin M on 22/11/2020.
//  Copyright © 2020 DataDorf. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class StoreMapViewController: BaseViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var storeListTableview: UITableView!
    
    @IBOutlet weak var storeListView: UIView!
    
    @IBOutlet weak var lblStoreTitle: UILabel!
    @IBOutlet weak var lblStoreAddress: UILabel!
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    
    @IBOutlet weak var overlayView: UIVisualEffectView!
    var shopArray:[String:[storeStruct]] = [:]
    var emirateArray:[String] = []
    var shopList:[[String]] = []
    var storeData:StoreModel?
    var storesList:[storeStruct]?
    var markers = [GMSMarker]()
    
    var currentLat: Double = 0
    var currentLong: Double = 0
    var selectedLong:String = "0"
    var selectedLat:String = "0"
    
    @IBOutlet weak var imgAtm: UIImageView!
    @IBOutlet weak var imgPray: UIImageView!
    @IBOutlet weak var imgPark: UIImageView!
    
    
    private var locationManager:CLLocationManager?
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        self.overlayView.isHidden = true
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
        self.mapView.isHidden = false
        self.mapView.delegate = self
        self.storeListView.isHidden = true
       // self.getStoresList()
       
    }
    
    @IBAction func mapChange(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            self.mapView.isHidden = false
            self.storeListView.isHidden = true
        case 1:
            self.mapView.isHidden = true
            self.storeListView.isHidden = false
        default:
            break
        }
    }
    
    @IBAction func overlayViewCancel(_ sender: Any) {
        self.overlayView.isHidden = true
    }
    @IBAction func directionPressed(_ sender: Any) {
        
        if  UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")! as URL){
        UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(selectedLat)),\(selectedLong)&zoom=14&views=traffic&q=\(selectedLat),\(selectedLong)")!, options: [:], completionHandler: nil)

             }else{


                  UIApplication.shared.openURL(NSURL(string: "itms://itunes.apple.com/us/app/google-maps-transit-food/id585027354?mt=8")! as URL)
        }

    }
    
    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    func getStoresList(lat:String,long:String){
        
        self.STProgress.show()
    
        
        ApiRequest.getStoreList(withParameter:["customerLatitude":lat,"customerLongitude":long]) { (isSuccess,message) in
            
            self.STProgress.dismiss()
            
            if isSuccess {
                
             
                if let info = self.ApiRequest.StoreListResponse?.stores{
                    
                    
                    
                    if info.count > 0{
                        
                        self.storesList = info
                        
           
                        for data in info
                        {
                            

                            if let location = data.storeLocation{
                             
                                self.emirateArray.append(location)
                            }
                  
                        }
                        
                      
                        
                        for shop in self.storesList! {
                            
                            
                            let long = Double(shop.storeLongitude!)!
                            let lat = Double(shop.storeLatitude!)!

                            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 14)
                            self.mapView?.camera = camera
                            self.mapView?.animate(to: camera)
                            
                                let marker = GMSMarker()
                                marker.position = CLLocationCoordinate2DMake(lat,long)
                                marker.icon = UIImage(named: "mPin")
                                marker.map = self.mapView
                                self.markers.append(marker)
//                                print ("Your GMSMarker")
//                                print (marker)

                            }
                        
                        //print(self.storesList)
                        
                        if self.storesList?.count ?? 0 > 0{
                            
                            self.storeListTableview.delegate = self
                            self.storeListTableview.dataSource = self
                            self.storeListTableview.reloadData()
                            
                        }
                      
//
                        
                    }
                 
             }
                
                
               
                
            }
            
        }
        
        
    }
   

}


extension StoreMapViewController:GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        if let index = markers.firstIndex(of: marker) {
           
            if let info = self.storesList?[index]{
                
                self.lblStoreTitle.text = info.storeName
                self.lblArea.text = info.storeLocation
                self.lblStoreAddress.text = info.storeAddress
                self.lblDistance.text = "\(info.distanceInKm ?? 0)" + "KM"
                self.lblPhone.text = info.storeContact
                self.selectedLat = info.storeLatitude ?? "0"
                self.selectedLong = info.storeLongitude
                    ?? "0"
                
                if let pray = info.isPrayerRoom{
                    if let park = info.isParking{
                        if let atm = info.isATM{
                            print("****",park,pray,atm)
                            self.setFetures(pray:pray, park:park, atm:atm )
                        }
                    }
                }
                
                self.overlayView.isHidden = false
                
            }
            
            
            
            
           }
        return true
    }
}

extension StoreMapViewController:UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate{
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let info = self.storesList?[indexPath.row]{
            
            self.lblStoreTitle.text = info.storeName
            self.lblArea.text = info.storeLocation
            self.lblStoreAddress.text = info.storeAddress
            self.lblDistance.text = "\(info.distanceInKm ?? 0)" + "KM"
            self.lblPhone.text = info.storeContact
            self.selectedLat = info.storeLatitude ?? "0"
            self.selectedLong = info.storeLongitude
                ?? "0"
            
            if let pray = info.isPrayerRoom{
                if let park = info.isParking{
                    if let atm = info.isATM{
                        print("****",park,pray,atm)
                        self.setFetures(pray:pray, park:park, atm:atm )
                    }
                }
            }
            
            self.overlayView.isHidden = false
            
        }
        
    }
    
    func setFetures(pray:String,park:String,atm:String){
        self.imgAtm.isHidden = true
        self.imgPray.isHidden = true
        self.imgPark.isHidden = true
        if pray == "1"{
            self.imgPray.isHidden = false
        }
        if park == "1"{
            self.imgPark.isHidden = false
        }
        if atm == "1"{
            self.imgAtm.isHidden = false
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.storesList?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.storeListTableview.dequeueReusableCell(withIdentifier: "storeCell", for: indexPath) as! StoreTableViewCell
        
        
        var shopLat = 0.0
        var shopLong = 0.0
        
        if let info = self.storesList?[indexPath.row]{
            cell.lblTitle.text = info.storeName
            cell.lblDesc.text = info.storeAddress
            cell.lblLocation.text = info.storeLocation
            if let lat = info.storeLatitude{
                shopLat = Double(lat)!
            }
           
            if let long = info.storeLongitude{
                shopLong = Double(long)!
            }
            
            if let distance = info.distanceInKm{
                cell.lblDistance.text = "\(distance) " + "KM"
            }
            
        }

        
        return cell
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        
     
                              if let location = locations.last {
                      
                                  if location != nil{
                      
                      
                                      self.currentLat = location.coordinate.latitude
                                      self.currentLong = location.coordinate.longitude
                                    
                                  }
                      
                              }
                              self.locationManager?.stopUpdatingLocation()
                              self.locationManager?.delegate = nil
        self.getStoresList(lat: "\(self.currentLat)", long: "\(self.currentLong)")
                             

      }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("location manager authorization status changed")
        
        switch status {
        case .authorizedAlways:
            
            locationManager?.startUpdatingLocation()
            locationManager?.delegate = self
            
           // retriveCurrentLocation()
            print("user allow app to get location data when app is active or in background")
        case .authorizedWhenInUse:
            
            locationManager?.startUpdatingLocation()
            locationManager?.delegate = self
            
            print("user allow app to get location data only when app is active")
        case .denied:
            
             locationManager?.requestAlwaysAuthorization()
             self.STProgress.dismiss()
            
            print("user tap 'disallow' on the permission dialog, cant get location data")
        case .restricted:
            
            self.STProgress.dismiss()
           
            print("parental control setting disallow location data")
        case .notDetermined:
            
             self.STProgress.dismiss()
            
            print("the location permission dialog haven't shown before, user haven't tap allow/disallow")
        default:
            break
        }
    }
    
    
}
