//
//  ContactUsViewController.swift
//  Talal Market
//
//  Created by Sajin M on 22/11/2020.
//  Copyright © 2020 DataDorf. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func backPressed(_ sender: Any) {
        
        self.backNavigation()
    }
    
    
    @IBAction func fbPressed(_ sender: Any) {
        
        openFacebook()
    }
    
    @IBAction func instaPressed(_ sender: Any) {
     
        openInstagram()
    }
    
 
    @IBAction func gmailPressed(_ sender: Any) {
        
        sendEmail()
    }
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["info@talalgroupintl.com"])
           

            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }

    
    func openFacebook() {
             guard let url = URL(string: "fb://profile/1471646323135907")  else { return }
             if UIApplication.shared.canOpenURL(url) {
                 if #available(iOS 10.0, *) {
                     UIApplication.shared.open(url, options: [:], completionHandler: nil)
                 } else {
                     UIApplication.shared.openURL(url)
                 }
             }
         }
    
    
         func openInstagram() {
             guard let url = URL(string: "https://instagram.com/talal_market/?igshid=z0nd91yj3gtw")  else { return }
             if UIApplication.shared.canOpenURL(url) {
                 if #available(iOS 10.0, *) {
                     UIApplication.shared.open(url, options: [:], completionHandler: nil)
                 } else {
                     UIApplication.shared.openURL(url)
                 }
             }
         }
         
    
}
extension ContactUsViewController:MFMailComposeViewControllerDelegate{
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
}


