//
//  PromotionViewCell.swift
//  Talal Market
//
//  Created by Sajin M on 11/09/2020.
//  Copyright © 2020 DataDorf. All rights reserved.
//

import UIKit

class PromotionViewCell: UICollectionViewCell {
    
  
        
        @IBOutlet weak var imgBanner: UIImageView!
        @IBOutlet weak var lblTitle: UILabel!
        @IBOutlet weak var lblTime: UILabel!
        
   
}
