//
//  NameTableCell.swift
//  Talal Market
//
//  Created by Sajin M on 12/09/2020.
//  Copyright © 2020 DataDorf. All rights reserved.
//

import UIKit

class NameTableCell: UITableViewCell {
    
    
    @IBOutlet weak var lblStoreName: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
