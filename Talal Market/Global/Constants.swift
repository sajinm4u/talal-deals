//
//  Constants.swift
//  Talal Market
//
//  Created by Sajin M on 09/09/2020.
//  Copyright © 2020 DataDorf. All rights reserved.
//

import Foundation
import UIKit


let BaseUrl:String =  "https://deals.talalgroupintl.com/api/"

//User Defaults

let defualts = UserDefaults.standard


let bannerCell:String = "cell"

//Google API key

let GoogleKey:String = "AIzaSyD-nLT-hTw-3Hzutg2gYSeWAzri93Tsrw0"
let googleMapKey:String = "AIzaSyCjjgkmOc6Q2XWKj4-mK9NcDJsJsjZG6xY"
let phImage = UIImage(named:"ph")
let talalPh = UIImage(named:"talalPh")



//App Alert
let AppName = "Talal Market"

let noInternetTitle = "No Internet Connection"
let noInternetMessage = "Please check your internet connection."
let notValidEmail = "Please enter a valid email id"
let fillAllFields = "Please fill all the fields"
let cannotConnect = "Can't connect to server. Please try again later."
let notValidPhone = "Phone number is not valid"


enum Colors {
    
    static let NOGreen =  UIColor(red: 72/255, green: 158/255, blue: 89/255, alpha: 1.0)
    static let NOGray =  UIColor(red: 56/255, green: 56/255, blue: 56/255, alpha: 1.0)
}



enum Api{


static let banners = BaseUrl + "storeBannersList"
static let fcmRegister = BaseUrl + "fcmRegister"
static let promotionByLocation = BaseUrl + "get_promotions_by_location"
static let byStoreId = BaseUrl + "get_promotions_by_store_id"
static let byCategory = BaseUrl + "get_promotions_by_category_type"
static let storeList = BaseUrl + "storesList"
static let feedback = BaseUrl + "customerFeedback"

}
