//
//  Parameters.swift
//  Talal Market
//
//  Created by Sajin M on 10/09/2020.
//  Copyright © 2020 DataDorf. All rights reserved.
//

import Foundation



struct FcmRegisterParam {
    
    var fcm_regId:String = ""
     
     var Values: [String: String] {
           return [

               "deviceTypeId":"2",
               "userTypeId":"1",
               "fcmRegId":fcm_regId,
               "userId":"1"
               
               
           ]
       }
    
 
    
}

struct NearestParams{
    
    
    var lattitude:String = ""
    var longitude:String = ""
    
    
    var Values: [String: String] {
          return [
            
            "latitude":lattitude,
            "longitude":longitude
            
          ]
      }
    
    
}

struct FeedbackParams{
    
    
    var Name:String = ""
    var Mobile:String = ""
    var Email:String = ""
    var Message:String = ""
    
    
    var Values: [String: String] {
          return [
            
            "customerName":Name,
            "customerMobile":Mobile,
            "customerEmailId":Email,
            "customerMessage":Message
            
          ]
      }
    
    
}


struct FlyerParams{
    
    
    var store_id:String = ""
    var category_id:String = ""
    
    
    var Values: [String: String] {
          return [
            
            "store_id":store_id,
            "category_id":category_id
            
          ]
      }
    
    
}
