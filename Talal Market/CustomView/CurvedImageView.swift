//
//  ImageViewExt.swift

//
//  Created by Sajin M on 17/05/2020.
//  Copyright © 2020 Netstager. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class CurvedImage: UIImageView {
    
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor = .clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    
    override func awakeFromNib() {
          super.awakeFromNib()
         
      }
      override init(frame: CGRect) {
          super.init(frame: frame)
         
      }
      
      required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
      }
    
}

