//
//  UINavigationExt.swift
//  Nesto OMAN
//
//  Created by Sajin M on 15/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
  func popToViewController(ofClass: AnyClass, animated: Bool = true) {
    if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
      popToViewController(vc, animated: animated)
    }
  }
}
