//
//  UIFontExtension.swift
//  Nesto OMAN
//
//  Created by Sajin M on 15/07/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {

    func withTraits(traits:UIFontDescriptor.SymbolicTraits...) -> UIFont {
        let descriptor = self.fontDescriptor
            .withSymbolicTraits(UIFontDescriptor.SymbolicTraits(traits))
        return UIFont(descriptor: descriptor!, size: 0)
    }

    func bold() -> UIFont {
        return withTraits(traits: .traitBold)
    }
}
