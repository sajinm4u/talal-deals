//
//  DoubleExt.swift
//  Nesto OMAN
//
//  Created by Sajin M on 23/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation

extension Double{

      func roundToDecimal(_ fractionDigits: Int) -> Double {
           let multiplier = pow(10, Double(fractionDigits))
           return Darwin.round(self * multiplier) / multiplier
       }
    
    
}
