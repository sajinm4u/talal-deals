//
//  CommonModel.swift
//  Talal Market
//
//  Created by Sajin M on 10/09/2020.
//  Copyright © 2020 DataDorf. All rights reserved.
//

import Foundation

struct CommonModel:Codable {
    var status:Bool
    var message:String
    var regId:String?
}
