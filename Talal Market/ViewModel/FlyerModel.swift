//
//  FlyerModel.swift
//  Talal Market
//
//  Created by Sajin M on 11/09/2020.
//  Copyright © 2020 DataDorf. All rights reserved.
//

import Foundation


struct FlyerModel:Codable{
    
    var status:Bool
    var store_id:String?
    var store_name:String?
    var image_path:[String]?

}
