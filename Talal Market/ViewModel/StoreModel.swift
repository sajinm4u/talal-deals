//
//  StoreModel.swift
//  Talal Market
//
//  Created by Sajin M on 11/09/2020.
//  Copyright © 2020 DataDorf. All rights reserved.
//

import Foundation


struct StoreModel:Codable {
    
       var status:Bool
       var store_id:String?
       var store_name:String?
       var store_address:String?
       var latitude:String?
       var longitude:String?
       var arrOfferCategory:[storePromoData]?
    
}


struct storePromoData:Codable {
        
    var id:Int?
    var categoryId:Int?
    var promoCategory:String?
    var promoImagePath:String?
    var promoTillDate:String?
    
    }
    
   
