//
//  BannerModel.swift
//  Nesto OMAN
//
//  Created by Sajin M on 18/06/2020.
//  Copyright © 2020 Nesto. All rights reserved.
//

import Foundation


struct BannerModel:Codable{
    
    
    var result:Bool
    var message:String?
    var banners:[BannerData]?
    
    
}

struct BannerData:Codable{
    
    var id:String?
    var bannerImage:String?
    
}



