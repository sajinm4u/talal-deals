//
//  StoresListModel.swift
//  Talal Market
//
//  Created by Sajin M on 12/09/2020.
//  Copyright © 2020 DataDorf. All rights reserved.
//

import Foundation


struct StoresListModel:Codable {

    var result:Bool
    var message:String
    var stores:[storeStruct]?
    
}



struct storeStruct:Codable{
    
    
    var id:String?
    var storeCode:String?
    var storeName:String?
    var storeAddress:String?
    var storeLocation:String?
    var storeArea:String?
    var storeImage:String?
    var storeLatitude:String?
    var storeLongitude:String?
    var isATM:String?
    var isPrayerRoom:String?
    var isParking:String?
    var storeContact:String?
    var distanceInKm:Double?
    
    
}
