//
//  ResponseHandler.swift
//  Riddle Book
//
//  Created by Sajin M on 6/24/19.
//  Copyright © 2019 Sweans. All rights reserved.
//

import Foundation
import UIKit

class ResponseHandler {
    

    
var BannerResponse: [BannerData]?
var NearestLocationResponse:StoreModel?
var FlyerResponse:[String]?
var StoreListResponse:StoresListModel?
    

    public func registerFCM(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
           
           guard let url = URL(string:Api.fcmRegister) else {return}
           
           
           ApiManager.shared.confirmOrderData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, message) in
               
               if isSuccess{
                   
                   completion(true,message)
                   
               }
                   
               else {
                   completion(false,message)
               }
           })
       }
    
    
    
    public func getStoreList(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
           
           guard let url = URL(string:Api.storeList) else {return }
           
           
           ApiManager.shared.getStoreListData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, storeData, message) in
               
               if isSuccess{
                   
            
                   self.StoreListResponse = storeData
                       
                   completion(true,message)
                       
                   
               }
                   
               else {
                   completion(false,message)
               }
           })
       }
    
    public func getFlyers(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.byCategory) else {return }
        
        
        ApiManager.shared.getFlyerData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, FlyerData, message) in
            
            if isSuccess{
                
           
                    
                self.FlyerResponse = FlyerData?.image_path
                    
                completion(true,message)
                    
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    public func sendFeedback(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.feedback) else {return }
        
        
        ApiManager.shared.submitFeedBack(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, message) in
            
            if isSuccess{
                
               
                    completion(true,message)
                    
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    public func getStore(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.byStoreId) else {return }
        
        
        ApiManager.shared.getStoreWithId(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, LocationData, message) in
            
            if isSuccess{
                
                if LocationData!.status {
                    
                    self.NearestLocationResponse = LocationData
                    
                    completion(true,message)
                    
                    
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
    public func getNearestLocation(withParameter theParameter:[String : String], completion: @escaping ((Bool,String) -> Void)) {
        
        guard let url = URL(string:Api.promotionByLocation) else {return }
        
        
        ApiManager.shared.getNearestLocationData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, LocationData, message) in
            
            if isSuccess{
                
                if LocationData!.status {
                    
                    self.NearestLocationResponse = LocationData
                    
                    completion(true,message)
                    
                    
                    
                }
                
                
            }
                
            else {
                completion(false,message)
            }
        })
    }
    
   
    
    public func getBanner(withParameter theParameter:[String : String], completion: @escaping ((Bool,[BannerData]?,String) -> Void)) {
        
        guard let url = URL(string:Api.banners) else {return }
        
        
        ApiManager.shared.getBannerData(fromUrl: url,withParameter:theParameter, completion: { (isSuccess, bannerData, message) in
            
            if isSuccess{
                
                if bannerData!.result {
                    
                   
                    completion(true,(bannerData?.banners)!,message)
                    
                    
                    
                }
                
                
            }
                
            else {
                
                let banner = [BannerData]()
                
                completion(false,banner,message)
            }
        })
    }
    
    
    
}
