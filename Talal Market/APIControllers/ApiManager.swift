//
//  ApiManager.swift
//  Riddle Book
//
//  Created by Sajin M on 6/24/19.
//  Copyright © 2019 Sweans. All rights reserved.
//

import Foundation




class ApiManager {
    
       private init () {}
       static let shared = ApiManager()
    
       let ApiHandler = RestHandler()
    
    
    func getHeader() -> [String:String] {
        
              if let token = defualts.string(forKey:"token") {
                  
                  
                  
                  let tokenString = token
                
                   return ["Authorization":tokenString]
              }
              
              return ["":""]
              
              
          }
    
    


    
    
//    func getNearestLocationData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,StoreModel?,String) -> Void))  {
//
//
//
//             ApiHandler.request(toURL: url, method: .get,parameters: withParameter) { (response) in
//
//
//
//                 do {
//
//                   if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
//
//
//                     let decoder = JSONDecoder()
//                     let model = try decoder.decode(StoreModel.self, from:
//                         response.data!)
//
//
//                     DispatchQueue.main.async {
//                        completion(model.status,model,model.message)
//                     }
//
//
//                   }else{
//
//                       DispatchQueue.main.async {
//                           completion(false,nil,cannotConnect)
//                       }
//
//                   }
//
//
//
//                 } catch let parsingError {
//
//                    print(parsingError)
//
//                     let model:StoreModel? = nil
//
//                     DispatchQueue.main.async {
//                         completion(false,model,cannotConnect)
//                     }
//
//                 }
//
//
//
//             }
//
//
//         }
    

    


    
//    func getSelectedLocation(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,GroceryLocationModel?,String) -> Void))  {
//
//
//
//             ApiHandler.request(toURL: url, method: .get,parameters: withParameter) { (response) in
//
//
//
//                 do {
//
//                   if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
//
//
//                     let decoder = JSONDecoder()
//                     let model = try decoder.decode(GroceryLocationModel.self, from:
//                         response.data!)
//
//
//                     DispatchQueue.main.async {
//                        completion(model.status,model,model.message)
//                     }
//
//
//                   }else{
//
//                       DispatchQueue.main.async {
//                           completion(false,nil,cannotConnect)
//                       }
//
//                   }
//
//
//
//                 } catch let parsingError {
//
//                    print(parsingError)
//
//                     let model:GroceryLocationModel? = nil
//
//                     DispatchQueue.main.async {
//                         completion(false,model,cannotConnect)
//                     }
//
//                 }
//
//
//
//             }
//
//
//         }

    

    


    
    
//
//    func getLocationData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,AreaModel?,String) -> Void))  {
//
//
//
//          ApiHandler.request(toURL: url, method: .get,parameters: withParameter) { (response) in
//
//
//
//              do {
//
//                if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
//
//                  let decoder = JSONDecoder()
//                  let model = try decoder.decode(AreaModel.self, from:
//                      response.data!)
//                  DispatchQueue.main.async {
//                    completion(model.status,model,model.message)
//                  }
//                }else{
//
//                    let model:AreaModel? = nil
//
//                    DispatchQueue.main.async {
//                            completion(false,model,cannotConnect)
//                                       }
//
//
//                }
//
//
//
//              } catch let parsingError {
//                  let model:AreaModel? = nil
//
//                print(parsingError)
//
//                  DispatchQueue.main.async {
//                      completion(false,model,cannotConnect)
//                  }
//
//              }
//
//
//
//          }
//
//
//      }
    
    
    
    func getStoreListData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,StoresListModel?,String) -> Void))  {



        ApiHandler.request(toURL: url, method:.get ,parameters: withParameter) { (response) in



            do {
                
                print(url)

              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {


                let decoder = JSONDecoder()
                let model = try decoder.decode(StoresListModel.self, from:
                    response.data!)


                DispatchQueue.main.async {
                   completion(model.result,model,"")
                }


              }else{

                  DispatchQueue.main.async {
                      completion(false,nil,cannotConnect)
                  }

              }



            } catch let parsingError {

               print(parsingError)

                let model:StoresListModel? = nil

                DispatchQueue.main.async {
                    completion(false,model,cannotConnect)
                }

            }



        }


    }
    
    
    func getFlyerData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,FlyerModel?,String) -> Void))  {



        ApiHandler.request(toURL: url, method:.get ,parameters: withParameter) { (response) in



            do {

              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {


                let decoder = JSONDecoder()
                let model = try decoder.decode(FlyerModel.self, from:
                    response.data!)


                DispatchQueue.main.async {
                   completion(model.status,model,"")
                }


              }else{

                  DispatchQueue.main.async {
                      completion(false,nil,cannotConnect)
                  }

              }



            } catch let parsingError {

               print(parsingError)

                let model:FlyerModel? = nil

                DispatchQueue.main.async {
                    completion(false,model,cannotConnect)
                }

            }



        }


    }
    
    
    func submitFeedBack(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,String) -> Void))  {



        ApiHandler.request(toURL: url, method:.get ,parameters: withParameter) { (response) in



            do {

              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {


                let decoder = JSONDecoder()
                let model = try decoder.decode(GeneralModel.self, from:
                    response.data!)


                DispatchQueue.main.async {
                    completion(model.result,model.message)
                }


              }else{

                  DispatchQueue.main.async {
                      completion(false,cannotConnect)
                  }

              }



            } catch let parsingError {

               print(parsingError)

                let model:GeneralModel? = nil

                DispatchQueue.main.async {
                    completion(false,cannotConnect)
                }

            }



        }


    }
    
    
    func getStoreWithId(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,StoreModel?,String) -> Void))  {



        ApiHandler.request(toURL: url, method:.get ,parameters: withParameter) { (response) in



            do {

              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {


                let decoder = JSONDecoder()
                let model = try decoder.decode(StoreModel.self, from:
                    response.data!)


                DispatchQueue.main.async {
                   completion(model.status,model,"")
                }


              }else{

                  DispatchQueue.main.async {
                      completion(false,nil,cannotConnect)
                  }

              }



            } catch let parsingError {

               print(parsingError)

                let model:StoreModel? = nil

                DispatchQueue.main.async {
                    completion(false,model,cannotConnect)
                }

            }



        }


    }
    
    func getNearestLocationData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,StoreModel?,String) -> Void))  {



        ApiHandler.request(toURL: url, method:.get ,parameters: withParameter) { (response) in



            do {

              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {


                let decoder = JSONDecoder()
                let model = try decoder.decode(StoreModel.self, from:
                    response.data!)


                DispatchQueue.main.async {
                   completion(model.status,model,"")
                }


              }else{

                  DispatchQueue.main.async {
                      completion(false,nil,cannotConnect)
                  }

              }



            } catch let parsingError {

               print(parsingError)

                let model:StoreModel? = nil

                DispatchQueue.main.async {
                    completion(false,model,cannotConnect)
                }

            }



        }


    }
    
    
    
    func confirmOrderData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,String) -> Void))  {
          

     
            ApiHandler.request(toURL: url, method: .post,parameters: withParameter) { (response) in
                
          
                
                do {
                    
                  if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                   
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(CommonModel.self, from:
                        response.data!)
                    DispatchQueue.main.async {
                        completion(model.status,model.message)
                    }
                  }else{
                      
                    
                      
                      DispatchQueue.main.async {
                              completion(false,cannotConnect)
                                         }
                      
                      
                  }
                  
                    
                    
                } catch let parsingError {
                    
                    
                  print(parsingError)
                    
                    DispatchQueue.main.async {
                        completion(false,cannotConnect)
                    }
                    
                }
                
                
                
            }
            
          
          
      }
    
    
    
    func getBannerData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,BannerModel?,String) -> Void))  {
          
          
          
          ApiHandler.request(toURL: url, method: .get,parameters: withParameter) { (response) in
              
              
              
              do {
                  
                if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                 
                  let decoder = JSONDecoder()
                  let model = try decoder.decode(BannerModel.self, from:
                      response.data!)
                  DispatchQueue.main.async {
                      completion(true,model,model.message ?? "")
                  }
                }else{
                    
                    let model:BannerModel? = nil
                    
                    DispatchQueue.main.async {
                            completion(false,model,cannotConnect)
                                       }
                    
                    
                }
                
                  
                  
              } catch let parsingError {
                  let model:BannerModel? = nil
                  
                print(parsingError)
                  
                  DispatchQueue.main.async {
                      completion(false,model,cannotConnect)
                  }
                  
              }
              
              
              
          }
          
          
      }
    
    
   
    
    /*
    
    func getSdkToken(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,TokenModel?,String) -> Void))  {
           
           
      
        ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeader()  ) { (response) in
               
               
               
               do {
                   
                 if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                  
                   let decoder = JSONDecoder()
                   let model = try decoder.decode(TokenModel.self, from:
                       response.data!)
                   DispatchQueue.main.async {
                       completion(true,model,model.message)
                   }
                 }else{
                     
                     DispatchQueue.main.async {
                                            completion(false,nil,cannotConnect.localized)
                                        }
                     
                     
                 }
                 
                   
                   
               } catch let parsingError {
                   let model:TokenModel? = nil
                   
                 print(parsingError)
                   
                   DispatchQueue.main.async {
                       completion(false,model,cannotConnect.localized)
                   }
                   
               }
               
               
               
           }
           
           
           
           
       }
    
    
    
    
    
    
    
    
    
    
    func getHomeData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,HomeDataModel?,String) -> Void))  {
          
          
          
          ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeaderLocale()) { (response) in
              
              
              
              do {
                  
                if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                 
                  let decoder = JSONDecoder()
                  let model = try decoder.decode(HomeDataModel.self, from:
                      response.data!)
                  DispatchQueue.main.async {
                      completion(true,model,model.message)
                  }
                }else{
                    
                    DispatchQueue.main.async {
                                           completion(false,nil,cannotConnect.localized)
                                       }
                    
                    
                }
                
                  
                  
              } catch let parsingError {
                  let model:HomeDataModel? = nil
                  
                print(parsingError)
                  
                  DispatchQueue.main.async {
                      completion(false,model,cannotConnect.localized)
                  }
                  
              }
              
              
              
          }
          
          
          
          
      }
    
    
    
    func getHotelRoomData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,RoomModel?,String) -> Void))  {
        
        print(withParameter)
        
          ApiHandler.requestFormData(toURL: url, method: .post,parameters: withParameter, headers:getHeaderLocale()) { (response) in
            
//            print(response)
//            
//            print(response.response?.httpStatusCode)
            
            if response.data != nil {

            
            do {
                              
                if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                  
    
                
//                 let jsonString = String(data: response.data!, encoding: .utf8)
//                 print("JSON String : " + jsonString!)
                
             
                let decoder = JSONDecoder()
                let model = try decoder.decode(RoomModel.self, from:
                    response.data!)
                DispatchQueue.main.async {
                    completion(true,model,model.message)
                }
                  
                  
              }else{
                  
                  DispatchQueue.main.async {
                      completion(false,nil,cannotConnect.localized)
                  }
                  
              }
                
                
                
            } catch let parsingError {
              
              print(parsingError)
              
                let model:RoomModel? = nil
                
              
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect.localized)
                }
                
            }
                
            }else{
                
                return
            }
            
            
            
        }
        
      
    }
    
    
    func getMealsTypeData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,CommonModel?,String) -> Void))  {
        
        
        
        ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeaderLocale()) { (response) in
            
     
            
            do {
                
              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                  
             
                let decoder = JSONDecoder()
                let model = try decoder.decode(CommonModel.self, from:
                    response.data!)
                DispatchQueue.main.async {
                    completion(true,model,model.message)
                }
                  
                  
              }else{
                  
                  DispatchQueue.main.async {
                    completion(false,nil,cannotConnect.localized)
                  }
                  
              }
                
                
                
            } catch let parsingError {
              
              print(parsingError)
              
                let model:CommonModel? = nil
                
              
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect.localized)
                }
                
            }
            
            
            
        }
        
      
    }
    
    
    func updateProfileData(fromUrl url: URL, withParameter:[String : Any], image:UIImage, completion: @escaping ((Bool,String) -> Void))  {
        
        
                Alamofire.upload(
                    multipartFormData: { multipartFormData in
                        
                        for (key,value) in withParameter {
                            if let value = value as? String {
                                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                            }
                        }
                        
                       
                            if  let imageData = image.jpegData(compressionQuality: 0.5) {
                                multipartFormData.append(imageData, withName: "profilepicture", fileName: "Image file", mimeType: "image/jpg")
                            }
                        
                },
                    to: url,
                    method: .post,
                    headers:getHeader(),
                    encodingCompletion: { encodingResult in
                        switch encodingResult {
                        case .success(let upload, _, _):
                            
                            upload.uploadProgress(closure: { (progress) in
                                print(progress)
                            })
                            
                            upload.responseJSON { response in
                                
                                // If the request to get activities is succesfull, store them
                                if response.result.isSuccess{
                                    print(response.debugDescription)
                                    
                                    
                                    
                                    
                                   // let jsonData = JSON(response.result.value!)
                                   // print(jsonData)
                                    
                                    DispatchQueue.main.async {
                                        completion(false,cannotConnect.localized.localized)
                                    }
                                    
                                } else {
                                    
                        
                                }
                                
                                //                        alert.dismiss(animated: true, completion:
                                //                            {
                                //                                self.dismiss(animated: true, completion:nil)
                                //
                                //                        })
                            }
                        case .failure(let encodingError):
                            
                           print("FALLE ------------")
        //                    print(encodingError)
                        }
                }
                )
        
        
//        ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeader()) { (response) in
//
//
//
//            do {
//
//              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
//
//
//                let decoder = JSONDecoder()
//                let model = try decoder.decode(CommonModel.self, from:
//                    response.data!)
//                DispatchQueue.main.async {
//                    completion(true,model,model.message)
//                }
//
//
//              }else{
//
//                  DispatchQueue.main.async {
//                      completion(false,nil,cannotConnect.localized)
//                  }
//
//              }
//
//
//
//            } catch let parsingError {
//
//              print(parsingError)
//
//                let model:CommonModel? = nil
//
//
//
//                DispatchQueue.main.async {
//                    completion(false,model,cannotConnect.localized)
//                }
//
//            }
//
//
//
//        }
        
      
    }
    
    
    func refundPolicyData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,RefundModel?,String) -> Void))  {
        
        
        
        ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeaderLocale()) { (response) in
            
     
            
            do {
                
              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                  
             
                let decoder = JSONDecoder()
                let model = try decoder.decode(RefundModel.self, from:
                    response.data!)
                DispatchQueue.main.async {
                    completion(true,model,model.message)
                }
                  
                  
              }else{
                  
                  DispatchQueue.main.async {
                      completion(false,nil,cannotConnect.localized)
                  }
                  
              }
                
                
                
            } catch let parsingError {
              
              print(parsingError)
              
                let model:RefundModel? = nil
                
              
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect.localized)
                }
                
            }
            
            
            
        }
        
      
    }
    
    
    func aboutUsPolicyData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,AboutUsModel?,String) -> Void))  {
        
        
        
        ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeaderLocale()) { (response) in
            
     
            
            do {
                
              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                  
             
                let decoder = JSONDecoder()
                let model = try decoder.decode(AboutUsModel.self, from:
                    response.data!)
                DispatchQueue.main.async {
                    completion(true,model,model.message)
                }
                  
                  
              }else{
                  
                  DispatchQueue.main.async {
                      completion(false,nil,cannotConnect.localized)
                  }
                  
              }
                
                
                
            } catch let parsingError {
              
              print(parsingError)
              
                let model:AboutUsModel? = nil
                
              
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect.localized)
                }
                
            }
            
            
            
        }
        
      
    }
    
    
    func getUpcomingFilterData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,UpcomingFilterModel?,String) -> Void))  {
        
        
        
        ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeaderLocale()) { (response) in
            
     
            
            do {
                
              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                  
             
                let decoder = JSONDecoder()
                let model = try decoder.decode(UpcomingFilterModel.self, from:
                    response.data!)
                DispatchQueue.main.async {
                    completion(true,model,model.message)
                }
                  
                  
              }else{
                  
                  DispatchQueue.main.async {
                      completion(false,nil,cannotConnect.localized)
                  }
                  
              }
                
                
                
            } catch let parsingError {
              
              print(parsingError)
              
                let model:UpcomingFilterModel? = nil
                
              
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect.localized)
                }
                
            }
            
            
            
        }
        
      
    }
    
    
    func contactUsData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,CommonModel?,String) -> Void))  {
        
        
        
        ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeaderLocale()) { (response) in
            
     
            
            do {
                
              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                  
             
                let decoder = JSONDecoder()
                let model = try decoder.decode(CommonModel.self, from:
                    response.data!)
                DispatchQueue.main.async {
                    completion(true,model,model.message)
                }
                  
                  
              }else{
                  
                  DispatchQueue.main.async {
                      completion(false,nil,cannotConnect.localized)
                  }
                  
              }
                
                
                
            } catch let parsingError {
              
              print(parsingError)
              
                let model:CommonModel? = nil
                
              
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect.localized)
                }
                
            }
            
            
            
        }
        
      
    }
    
    
    
    func changePasswordData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,CommonModel?,String) -> Void))  {
        
        
        
        ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeader()) { (response) in
            
     
            
            do {
                
              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                  
             
                let decoder = JSONDecoder()
                let model = try decoder.decode(CommonModel.self, from:
                    response.data!)
                DispatchQueue.main.async {
                    completion(true,model,model.message)
                }
                  
                  
              }else{
                  
                  DispatchQueue.main.async {
                      completion(false,nil,cannotConnect.localized)
                  }
                  
              }
                
                
                
            } catch let parsingError {
              
              print(parsingError)
              
                let model:CommonModel? = nil
                
              
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect.localized)
                }
                
            }
            
            
            
        }
        
      
    }
    
    
    func getRoomTypeData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,CommonModel?,String) -> Void))  {
        
        
        
        ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeaderLocale()) { (response) in
            
     
            
            do {
                
              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                  
             
                let decoder = JSONDecoder()
                let model = try decoder.decode(CommonModel.self, from:
                    response.data!)
                DispatchQueue.main.async {
                    completion(true,model,model.message)
                }
                  
                  
              }else{
                  
                  DispatchQueue.main.async {
                      completion(false,nil,cannotConnect.localized)
                  }
                  
              }
                
                
                
            } catch let parsingError {
              
              print(parsingError)
              
                let model:CommonModel? = nil
                
              
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect.localized)
                }
                
            }
            
            
            
        }
        
      
    }
    
    
    func getPackageListData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,PackageModel?,String) -> Void))  {
        
        
        
        ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeaderLocale()) { (response) in
            
     
            
            do {
                
              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                  
             
                let decoder = JSONDecoder()
                let model = try decoder.decode(PackageModel.self, from:
                    response.data!)
                DispatchQueue.main.async {
                    completion(true,model,model.message)
                }
                  
                  
              }else{
                  
                  DispatchQueue.main.async {
                      completion(false,nil,cannotConnect.localized)
                  }
                  
              }
                
                
                
            } catch let parsingError {
              
              print(parsingError)
              
                let model:PackageModel? = nil
                
              
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect.localized)
                }
                
            }
            
            
            
        }
        
      
    }
    
    
    
    func getHotelDetailData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,HotelDetailModel?,String) -> Void))  {
        
        
        
        ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeaderLocale()) { (response) in
            
     
            
            do {
                
              if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                  
             
                let decoder = JSONDecoder()
                let model = try decoder.decode(HotelDetailModel.self, from:
                    response.data!)
                DispatchQueue.main.async {
                    completion(true,model,model.message)
                }
                  
                  
              }else{
                  
                  DispatchQueue.main.async {
                      completion(false,nil,cannotConnect.localized)
                  }
                  
              }
                
                
                
            } catch let parsingError {
              
              print(parsingError)
              
                let model:HotelDetailModel? = nil
                
              
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect.localized)
                }
                
            }
            
            
            
        }
        
      
    }
    
    
    func getFilterData(fromUrl url: URL, withParameter:[String : Any], completion: @escaping ((Bool,HotelModel?,String) -> Void))  {
        
        
        
        Alamofire.request(url, method: .post, parameters: withParameter,encoding:JSONEncoding.default,  headers: getHeaderLocale()).responseJSON  { (response) in
            
     
            
            do {
                
                if (response.response!.statusCode >= 200 && response.response!.statusCode < 300) {
                  
             
                let decoder = JSONDecoder()
                let model = try decoder.decode(HotelModel.self, from:
                    response.data!)
                DispatchQueue.main.async {
                    completion(true,model,model.message)
                }
                  
                  
              }else{
                  
                  DispatchQueue.main.async {
                      completion(false,nil,cannotConnect.localized)
                  }
                  
              }
                
                
                
            } catch let parsingError {
              
              print(parsingError)
              
                let model:HotelModel? = nil
                
              
                
                DispatchQueue.main.async {
                    completion(false,model,cannotConnect.localized)
                }
                
            }
            
            
            
        }
        
      
    }
    
    
    func getHotelData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,HotelModel?,String) -> Void))  {
          
          
          
          ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeaderLocale()) { (response) in
              
       
              
              do {
                  
                if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                    
               
                  let decoder = JSONDecoder()
                  let model = try decoder.decode(HotelModel.self, from:
                      response.data!)
                  DispatchQueue.main.async {
                      completion(true,model,model.message)
                  }
                    
                    
                }else{
                    
                    DispatchQueue.main.async {
                        completion(false,nil,cannotConnect.localized)
                    }
                    
                }
                  
                  
                  
              } catch let parsingError {
                
                print(parsingError)
                
                  let model:HotelModel? = nil
                  
                
                  
                  DispatchQueue.main.async {
                      completion(false,model,cannotConnect.localized)
                  }
                  
              }
              
              
              
          }
          
        
      }
    
    
    
    func getDetailedData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,DetailsModel?,String) -> Void))  {
          
          
          
          ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeaderLocale()) { (response) in
              
       
              
              do {
                  
                if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                    
               
                  let decoder = JSONDecoder()
                  let model = try decoder.decode(DetailsModel.self, from:
                      response.data!)
                  DispatchQueue.main.async {
                      completion(true,model,model.message)
                  }
                    
                    
                }else{
                    
                    DispatchQueue.main.async {
                        completion(false,nil,cannotConnect.localized)
                    }
                    
                }
                  
                  
                  
              } catch _ {
                  let model:DetailsModel? = nil
                  
                
                  
                  DispatchQueue.main.async {
                      completion(false,model,cannotConnect.localized)
                  }
                  
              }
              
              
              
          }
          
        
      }
    
    
    func getTicketsData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,MyTicketsModel?,String) -> Void))  {
                
                
                
                ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeader()) { (response) in
                    
             
                    
                    do {
                        
                      if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                          
                     
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(MyTicketsModel.self, from:
                            response.data!)
                        DispatchQueue.main.async {
                            completion(true,model,model.message)
                        }
                          
                          
                      }else{
                          
                          DispatchQueue.main.async {
                              completion(false,nil,cannotConnect.localized)
                          }
                          
                      }
                        
                        
                        
                    } catch _ {
                        let model:MyTicketsModel? = nil
                        
                        DispatchQueue.main.async {
                            completion(false,model,cannotConnect.localized)
                        }
                        
                    }
                    
                    
                    
                }
                
              
            }
    
    func getTeamsMatchList(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,TeamMatchModel?,String) -> Void))  {
                   
                   
                   
                   ApiHandler.request(toURL: url, method: .post,parameters: withParameter) { (response) in
                       
                
                       
                       do {
                           
                         if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                             
                        
                           let decoder = JSONDecoder()
                           let model = try decoder.decode(TeamMatchModel.self, from:
                               response.data!)
                           DispatchQueue.main.async {
                               completion(true,model,model.message)
                           }
                             
                             
                         }else{
                             
                             DispatchQueue.main.async {
                                 completion(false,nil,cannotConnect.localized)
                             }
                             
                         }
                           
                           
                           
                       } catch _ {
                           let model:TeamMatchModel? = nil
                           
                           DispatchQueue.main.async {
                               completion(false,model,cannotConnect.localized)
                           }
                           
                       }
                       
                       
                       
                   }
                   
                 
               }
    
    
    func getTeamsList(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,TeamModel?,String) -> Void))  {
                
                
                
                ApiHandler.request(toURL: url, method: .post,parameters: withParameter) { (response) in
                    
             
                    
                    do {
                        
                      if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                          
                     
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(TeamModel.self, from:
                            response.data!)
                        DispatchQueue.main.async {
                            completion(true,model,model.message)
                        }
                          
                          
                      }else{
                          
                          DispatchQueue.main.async {
                              completion(false,nil,cannotConnect.localized)
                          }
                          
                      }
                        
                        
                        
                    } catch _ {
                        let model:TeamModel? = nil
                        
                        DispatchQueue.main.async {
                            completion(false,model,cannotConnect.localized)
                        }
                        
                    }
                    
                    
                    
                }
                
              
            }
    
    
    func getProfileData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,ProfileModel?,String) -> Void))  {
                
                
                
                ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeader()) { (response) in
                    
             
                    
                    do {
                        
                      if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                          
                     
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(ProfileModel.self, from:
                            response.data!)
                        DispatchQueue.main.async {
                            completion(true,model,model.message)
                        }
                          
                          
                      }else{
                          
                          DispatchQueue.main.async {
                              completion(false,nil,cannotConnect.localized)
                          }
                          
                      }
                        
                        
                        
                    } catch _ {
                        let model:ProfileModel? = nil
                        
                        DispatchQueue.main.async {
                            completion(false,model,cannotConnect.localized)
                        }
                        
                    }
                    
                    
                    
                }
                
              
            }
    
    
    func getBookingData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,MyBookingModel?,String) -> Void))  {
                  
                  
                  
                  ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeader()) { (response) in
                      
               
                      
                      do {
                          
                        if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                            
                       
                          let decoder = JSONDecoder()
                          let model = try decoder.decode(MyBookingModel.self, from:
                              response.data!)
                          DispatchQueue.main.async {
                              completion(true,model,model.message)
                          }
                            
                            
                        }else{
                            
                            DispatchQueue.main.async {
                                completion(false,nil,cannotConnect.localized)
                            }
                            
                        }
                          
                          
                          
                      } catch let parsingError {
                          let model:MyBookingModel? = nil
                          print(parsingError)
                          DispatchQueue.main.async {
                              completion(false,model,cannotConnect.localized)
                          }
                          
                      }
                      
                      
                      
                  }
                  
                
              }
    
    
    
    func getPackageData(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,MyPackageModel?,String) -> Void))  {
                
                
                
                ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeader()) { (response) in
                    
             
                    
                    do {
                        
                      if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                          
                     
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(MyPackageModel.self, from:
                            response.data!)
                        DispatchQueue.main.async {
                            completion(true,model,model.message)
                        }
                          
                          
                      }else{
                          
                          DispatchQueue.main.async {
                              completion(false,nil,cannotConnect.localized)
                          }
                          
                      }
                        
                        
                        
                    } catch let parsingError  {
                        
                        print(parsingError)
                        let model:MyPackageModel? = nil
                        
                        DispatchQueue.main.async {
                            completion(false,model,cannotConnect.localized)
                        }
                        
                    }
                    
                    
                    
                }
                
              
            }
    
    
    func savePayment(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,OfferDetailModel?,String) -> Void))  {
                
                
                
                ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeader()) { (response) in
                    
             
                    
                    do {
                        
                      if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                          
                     
                        let decoder = JSONDecoder()
                        let model = try decoder.decode(OfferDetailModel.self, from:
                            response.data!)
                        DispatchQueue.main.async {
                            completion(true,model,model.message)
                        }
                          
                          
                      }else{
                          
                          DispatchQueue.main.async {
                              completion(false,nil,cannotConnect.localized)
                          }
                          
                      }
                        
                        
                        
                    } catch _ {
                        let model:OfferDetailModel? = nil
                        
                        DispatchQueue.main.async {
                            completion(false,model,cannotConnect.localized)
                        }
                        
                    }
                    
                    
                    
                }
                
              
            }
    
    
    func getOfferDetails(fromUrl url: URL, withParameter:[String : String], completion: @escaping ((Bool,OfferDetailModel?,String) -> Void))  {
             
             
             
             ApiHandler.request(toURL: url, method: .post,parameters: withParameter, headers:getHeaderLocale()) { (response) in
                 
          
                 
                 do {
                     
                   if (response.response!.httpStatusCode >= 200 && response.response!.httpStatusCode < 300) {
                       
                  
                     let decoder = JSONDecoder()
                     let model = try decoder.decode(OfferDetailModel.self, from:
                         response.data!)
                     DispatchQueue.main.async {
                         completion(true,model,model.message)
                     }
                       
                       
                   }else{
                       
                       DispatchQueue.main.async {
                           completion(false,nil,cannotConnect.localized)
                       }
                       
                   }
                     
                     
                     
                 } catch _ {
                     let model:OfferDetailModel? = nil
                     
                     DispatchQueue.main.async {
                         completion(false,model,cannotConnect.localized)
                     }
                     
                 }
                 
                 
                 
             }
             
           
         }
       
    
    
    */
    
 

    
    }
